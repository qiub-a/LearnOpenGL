PROJECT(Breakout)

MESSAGE("------------------------Breakout")


INCLUDE_DIRECTORIES(
	${GULT_DIR}/include
	${GLEW_DIR}/include
	${GLFW_DIR}/include
	${GLAD_DIR}/include
	${GLAD_DIR}/src
	${GLM_DIR}
	${GULTMAIN_DIR}
	${SOIL_DIR}/include
	.
    )

LINK_DIRECTORIES(
	${GULT_DIR}/lib/${PlatformName}/${CMAKE_CFG_INTDIR}
	${GLEW_DIR}/lib/vc14/${PlatformName}/${CMAKE_CFG_INTDIR}
	${GLFW_DIR}/${PlatformName}/${CMAKE_CFG_INTDIR}
	${ASSIMP_DIR}
	${SOIL_DIR}/vc14/${PlatformName}/${CMAKE_CFG_INTDIR}
)

FILE(GLOB PROJECT_SOURCE_FILES *.txt)
FILE(GLOB PROJECT_SOURCE_FILES ${PROJECT_SOURCE_FILES}
	*.cpp
	*.h
	#${GLAD_DIR}/src/*.c
    )

ADD_EXECUTABLE(Breakout ${PROJECT_SOURCE_FILES})
IF (CMAKE_BUILD_TYPE MATCHES "Debug" ) 
    TARGET_LINK_LIBRARIES(Breakout freeglutd glew32sd)
ENDIF ()

IF (CMAKE_BUILD_TYPE MATCHES "Release")  
	TARGET_LINK_LIBRARIES(Breakout freeglut glew32s)
ENDIF ()

MESSAGE("------------------------${CMAKE_BUILD_TYPE}")


TARGET_LINK_LIBRARIES(Breakout opengl32 glfw3 assimp SOIL)

SET(EXECUTABLE_OUTPUT_PATH ${BIN_DIR})

SET_PROPERTY(TARGET Breakout  PROPERTY FOLDER "Tool")

install (TARGETS Breakout DESTINATION bin)

add_custom_command(TARGET Breakout POST_BUILD COMMAND xcopy \"${GULT_DIR}\\dll\\${PlatformName}\\Debug\\freeglutd.dll\" \"${BIN_DIR}/${CMAKE_CFG_INTDIR}\" /y)
add_custom_command(TARGET Breakout POST_BUILD COMMAND xcopy \"${GULT_DIR}\\dll\\${PlatformName}\\Release\\freeglut.dll\" \"${BIN_DIR}/${CMAKE_CFG_INTDIR}\" /y)
add_custom_command(TARGET Breakout POST_BUILD COMMAND xcopy \"${ASSIMP_DIR}\\assimp.dll\" \"${BIN_DIR}/${CMAKE_CFG_INTDIR}\" /y)
#add_custom_command(TARGET Breakout POST_BUILD COMMAND xcopy \"${GULT_DIR}\\${CMAKE_CFG_INTDIR}\\glut64.dll\" \"${BIN_DIR}/${CMAKE_CFG_INTDIR}\" /y)

