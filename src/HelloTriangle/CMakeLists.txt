PROJECT(HelloTriangle)

MESSAGE("------------------------HelloTriangle")


INCLUDE_DIRECTORIES(
	${GULT_DIR}/include
	${GLEW_DIR}/include
	${GLFW_DIR}/include
	${GLAD_DIR}/include
	${GLAD_DIR}/src
	${GLM_DIR}
	${GULTMAIN_DIR}
	${SOIL_DIR}/include
	.
    )

LINK_DIRECTORIES(
	${GULT_DIR}/lib/${PlatformName}/${CMAKE_CFG_INTDIR}
	${GLEW_DIR}/lib/vc14/${PlatformName}/${CMAKE_CFG_INTDIR}
	${GLFW_DIR}/${PlatformName}/${CMAKE_CFG_INTDIR}
	${ASSIMP_DIR}
)

FILE(GLOB PROJECT_SOURCE_FILES *.txt)
FILE(GLOB PROJECT_SOURCE_FILES ${PROJECT_SOURCE_FILES}
	*.cpp
	${GLAD_DIR}/src/*.c
    )

ADD_EXECUTABLE(HelloTriangle ${PROJECT_SOURCE_FILES})
#TARGET_LINK_LIBRARIES(HelloTriangle glut glut32)
IF (CMAKE_BUILD_TYPE MATCHES "Debug")  
    TARGET_LINK_LIBRARIES(HelloTriangle opengl32 freeglutd glew32d)
ENDIF ()

IF (CMAKE_BUILD_TYPE MATCHES "Release")  
	TARGET_LINK_LIBRARIES(HelloTriangle opengl32 freeglut glew32)
ENDIF ()

TARGET_LINK_LIBRARIES(HelloTriangle glfw3 assimp)

SET(EXECUTABLE_OUTPUT_PATH ${BIN_DIR})

SET_PROPERTY(TARGET HelloTriangle  PROPERTY FOLDER "Tool")

install (TARGETS HelloTriangle DESTINATION bin)

add_custom_command(TARGET HelloTriangle POST_BUILD COMMAND xcopy \"${GULT_DIR}\\dll\\${PlatformName}\\Debug\\freeglutd.dll\" \"${BIN_DIR}/${CMAKE_CFG_INTDIR}\" /y)
add_custom_command(TARGET HelloTriangle POST_BUILD COMMAND xcopy \"${GULT_DIR}\\dll\\${PlatformName}\\Release\\freeglut.dll\" \"${BIN_DIR}/${CMAKE_CFG_INTDIR}\" /y)
add_custom_command(TARGET HelloTriangle POST_BUILD COMMAND xcopy \"${ASSIMP_DIR}\\assimp.dll\" \"${BIN_DIR}/${CMAKE_CFG_INTDIR}\" /y)
#add_custom_command(TARGET HelloTriangle POST_BUILD COMMAND xcopy \"${GULT_DIR}\\${CMAKE_CFG_INTDIR}\\glut64.dll\" \"${BIN_DIR}/${CMAKE_CFG_INTDIR}\" /y)
